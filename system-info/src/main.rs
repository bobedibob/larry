mod params;

use etm::server::{MessageProcessing, Server};
use etm::Service;
use rpc::system::info;
use rpc::system::info::{
    ProtocolVersion, Request, Response, SERVICE_CONNECTION_REQUEST_PORT, SERVICE_ID,
};

use clap::Parser;
use sysinfo::{ComponentExt, CpuExt, CpuRefreshKind, NetworkExt, RefreshKind, System, SystemExt};

use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

struct LarrySystemInfo {
    data_miner_running: Arc<AtomicBool>,
    data_miner: Option<thread::JoinHandle<()>>,
    all_info: Arc<Mutex<info::AllSystemInfo>>,
}

impl LarrySystemInfo {
    fn new() -> Self {
        let params = params::Params::parse();
        log::debug!("{:?}", params);

        let all_info = Arc::new(Mutex::new(info::AllSystemInfo {
            ..Default::default()
        }));
        let data_miner_running = Arc::new(AtomicBool::new(true));

        let running = data_miner_running.clone();
        let info = all_info.clone();
        let data_miner = thread::spawn(move || {
            let refresh_kinds = RefreshKind::new()
                .with_components()
                .with_cpu(CpuRefreshKind::new().with_cpu_usage())
                .with_memory()
                .with_networks_list();
            let mut sys = System::new_with_specifics(refresh_kinds);

            while running.load(Ordering::Relaxed) {
                sys.refresh_specifics(refresh_kinds);

                if let Ok(ref mut info) = info.lock() {
                    info.uptime = sys.uptime();

                    info.cpu.clear();
                    info.cpu.push(sys.global_cpu_info().cpu_usage());
                    for cpu in sys.cpus() {
                        info.cpu.push(cpu.cpu_usage());
                    }

                    info.memory.total = sys.total_memory() * 1024;
                    info.memory.used = sys.used_memory() * 1024;
                    info.memory.swap = sys.total_swap() * 1024;
                    info.memory.swap_used = sys.used_swap() * 1024;

                    info.network.clear();
                    for (interface_name, network) in sys.networks() {
                        info.network.push(info::Network {
                            interface_name: interface_name.clone(),
                            received: network.received(),
                            transmitted: 0_u64,
                            packets_received: 0_u64,
                            packets_transmitted: 0_u64,
                            total_errors_on_received: 0_u64,
                            total_errors_on_transmitted: 0_u64,
                        });
                    }

                    info.temperature.clear();
                    for sensor in sys.components() {
                        info.temperature.push(info::Temperature {
                            label: sensor.label().to_string(),
                            value: sensor.temperature(),
                            max: sensor.max(),
                            critical: sensor.critical(),
                        });
                    }
                };

                thread::sleep(Duration::from_secs(1));
            }
        });

        LarrySystemInfo {
            data_miner_running,
            data_miner: Some(data_miner),
            all_info,
        }
    }

    fn stop_data_miner(&mut self) {
        self.data_miner_running.store(false, Ordering::Relaxed);
        if let Some(th) = self.data_miner.take() {
            if let Err(e) = th.join() {
                log::error!("{:?}", e);
            }
        };
    }
}

impl Drop for LarrySystemInfo {
    fn drop(&mut self) {
        self.stop_data_miner();
    }
}

impl MessageProcessing for LarrySystemInfo {
    type Rq = rpc::system::info::Request;
    type Rsp = rpc::system::info::Response;
    type E = rpc::system::info::Error;

    fn new() -> Arc<Self> {
        Arc::new(LarrySystemInfo::new())
    }

    fn execute(&self, _connection_id: u32, rpc: Self::Rq) -> Result<Self::Rsp, Self::E> {
        match rpc {
            Request::Ping() => Ok(Response::Pong()),
            Request::Info() => {
                let version_firmware =
                    env!("CARGO_PKG_NAME").to_string() + " - v" + env!("CARGO_PKG_VERSION");

                Ok(Response::Info(version_firmware))
            }
            Request::AllSystemInfo() => {
                if let Ok(ref info) = self.all_info.lock() {
                    Ok(Response::AllSystemInfo((*info).clone()))
                } else {
                    Err("could not get lock".to_string())
                }
            }
            Request::Uptime() => {
                if let Ok(ref info) = self.all_info.lock() {
                    Ok(Response::Uptime(info.uptime))
                } else {
                    Err("could not get lock".to_string())
                }
            }
            Request::CPU() => {
                if let Ok(ref info) = self.all_info.lock() {
                    Ok(Response::CPU(info.cpu.clone()))
                } else {
                    Err("could not get lock".to_string())
                }
            }
            Request::Memory() => {
                if let Ok(ref info) = self.all_info.lock() {
                    Ok(Response::Memory(info.memory))
                } else {
                    Err("could not get lock".to_string())
                }
            }
            Request::Network() => {
                if let Ok(ref info) = self.all_info.lock() {
                    Ok(Response::Network(info.network.clone()))
                } else {
                    Err("could not get lock".to_string())
                }
            }
            Request::Temperature() => {
                if let Ok(ref info) = self.all_info.lock() {
                    Ok(Response::Temperature(info.temperature.clone()))
                } else {
                    Err("could not get lock".to_string())
                }
            }
        }
    }
}

fn main() -> std::io::Result<()> {
    larry_log::init("info", larry_log::Target::DuplicateToConsole);

    let server = Server::<LarrySystemInfo>::new(
        SERVICE_CONNECTION_REQUEST_PORT,
        Service::entity(SERVICE_ID.to_string(), ProtocolVersion::entity().version()),
    );
    server.run()?;

    Ok(())
}
