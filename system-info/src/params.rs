use clap::Parser;

/// System Info for larry.
#[derive(Parser, Debug)]
#[command(author, version, about, long_about)]
pub struct Params {}
