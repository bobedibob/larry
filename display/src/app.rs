use rpc::system::info;

use std::net::Ipv4Addr;

use termion::event::MouseEvent;

type SysInfoConnection = etm::client::Connection<info::Request, info::Response, info::Error>;

pub struct App<'a> {
    pub title: &'a str,
    pub should_quit: bool,
    pub mouse_hold_position: Option<(u16, u16)>,
    pub debug: String,
    pub system_info: Option<info::AllSystemInfo>,
    sysinfo_connection: Option<Box<SysInfoConnection>>,
}

impl<'a> App<'a> {
    pub fn new(title: &'a str) -> App<'a> {
        App {
            title,
            should_quit: false,
            mouse_hold_position: None,
            debug: "".to_string(),
            system_info: None,
            sysinfo_connection: None,
        }
    }

    pub fn on_key(&mut self, c: char) {
        if let 'q' = c {
            self.should_quit = true;
        }
    }

    pub fn on_mouse(&mut self, m: MouseEvent) {
        match m {
            MouseEvent::Press(_, x, y) => self.mouse_hold_position = Some((x, y)),
            MouseEvent::Hold(x, y) => self.mouse_hold_position = Some((x, y)),
            _ => self.mouse_hold_position = None,
        }
        self.debug = format!("{:?}", m);
    }

    pub fn on_tick(&mut self) {
        if let Some(system_info) = self.get_all_sysinfo() {
            self.system_info = Some(system_info);
        }
    }

    pub fn connect_sysinfo(&mut self) {
        if self.sysinfo_connection.is_none() {
            if let Some(tmp_connection) = SysInfoConnection::new(
                Ipv4Addr::LOCALHOST,
                info::SERVICE_CONNECTION_REQUEST_PORT,
                1,
            ) {
                let service = etm::Service::entity(
                    info::SERVICE_ID.to_string(),
                    info::ProtocolVersion::entity().version(),
                );

                if tmp_connection.compatibility_check(service) {
                    self.sysinfo_connection = Some(tmp_connection);
                } else {
                    log::error!("Compatibility Check with system info daemon failed!");
                }
            }
        }
    }

    pub fn get_all_sysinfo(&mut self) -> Option<info::AllSystemInfo> {
        self.connect_sysinfo();
        if let Some(response) = self
            .sysinfo_connection
            .as_mut()?
            .transceive(info::Request::AllSystemInfo())
        {
            if let info::Response::AllSystemInfo(sysinfo) = response {
                return Some(sysinfo);
            }
        }
        None
    }
}
