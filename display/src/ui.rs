use rpc::system::info;

use std::io;

use termion::event::MouseEvent;

use ratatui::backend::Backend;
use ratatui::layout::{Constraint, Direction, Layout, Rect};
use ratatui::style::{Color, Modifier, Style};
use ratatui::text::{Line, Span};
use ratatui::widgets::{Block, Borders, Gauge, Paragraph, Wrap};
use ratatui::{Frame, Terminal};

use crate::App;

pub fn draw<B: Backend>(
    terminal: &mut Terminal<B>,
    app: &mut App,
    mouse_event: Option<MouseEvent>,
) -> Result<(), io::Error> {
    terminal.draw(|mut frame| {
        let sidebar_lenth = if let Ok((_, height)) = termion::terminal_size() {
            height / 2
        } else {
            15
        };
        let chunks = Layout::default()
            .constraints([Constraint::Length(sidebar_lenth), Constraint::Min(0)].as_ref())
            .direction(Direction::Horizontal)
            .split(frame.size());

        draw_left_sidebar(&mut frame, chunks[0], app, mouse_event);
        draw_main_view(&mut frame, chunks[1], app);
    })?;

    Ok(())
}

fn draw_left_sidebar<B>(
    frame: &mut Frame<B>,
    area: Rect,
    app: &App,
    _mouse_event: Option<MouseEvent>,
) where
    B: Backend,
{
    let height = area.height / 4;
    let chunks = Layout::default()
        .constraints(
            [
                Constraint::Min(0),
                Constraint::Length(height),
                Constraint::Length(height),
                Constraint::Length(height),
            ]
            .as_ref(),
        )
        .split(area);

    let point_in_rect = |x: u16, y: u16, rect: Rect| -> bool {
        x > rect.x && x < rect.x + rect.width && y > rect.y && y < rect.y + rect.height
    };

    //TODO: use mouse release event to trigger an action according to the mous event position

    let color = match app.mouse_hold_position {
        Some((x, y)) if point_in_rect(x, y, chunks[0]) => Color::Rgb(0xD9, 0x56, 0x56),
        _ => Color::Rgb(0xD9, 0x26, 0x26),
    };

    frame.render_widget(
        Block::default().style(Style::default().bg(color)),
        chunks[0],
    );

    let color = match app.mouse_hold_position {
        Some((x, y)) if point_in_rect(x, y, chunks[1]) => Color::Rgb(0x7D, 0xD9, 0x56),
        _ => Color::Rgb(0x59, 0xD9, 0x26),
    };

    frame.render_widget(
        Block::default().style(Style::default().bg(color)),
        chunks[1],
    );

    let color = match app.mouse_hold_position {
        Some((x, y)) if point_in_rect(x, y, chunks[2]) => Color::Rgb(0x56, 0xA3, 0xD9),
        _ => Color::Rgb(0x26, 0x8C, 0xD9),
    };

    frame.render_widget(
        Block::default().style(Style::default().bg(color)),
        chunks[2],
    );

    let color = match app.mouse_hold_position {
        Some((x, y)) if point_in_rect(x, y, chunks[3]) => Color::Rgb(0xD9, 0xC6, 0x56),
        _ => Color::Rgb(0xD9, 0xBF, 0x26),
    };

    frame.render_widget(
        Block::default().style(Style::default().bg(color)),
        chunks[3],
    );
}

fn draw_main_view<B>(frame: &mut Frame<B>, area: Rect, app: &App)
where
    B: Backend,
{
    if let Some(system_info) = app.system_info.as_ref() {
        let number_cpus = system_info.cpu.len();
        let number_memory = if system_info.memory.swap == 0 { 1 } else { 2 };
        let number_networks = system_info.network.len();
        let number_temp_sensors = system_info.temperature.len();

        let chunks = Layout::default()
            .constraints(
                [
                    Constraint::Length(3),
                    Constraint::Length(2 + number_cpus as u16),
                    Constraint::Length(2 + number_memory),
                    Constraint::Length(2 + 8 * number_networks as u16),
                    Constraint::Length(2 + number_temp_sensors as u16),
                    Constraint::Min(0),
                ]
                .as_ref(),
            )
            .split(area);

        draw_uptime(frame, chunks[0], system_info.uptime);
        draw_cpu(frame, chunks[1], &system_info.cpu);
        draw_memory(frame, chunks[2], &system_info.memory);
        draw_network(frame, chunks[3], &system_info.network);
    // draw_temperature(frame chunks[4], system_info);
    // draw_log(frame, chunks[5], app);
    } else {
        let chunks = Layout::default()
            .constraints([Constraint::Length(3), Constraint::Min(0)].as_ref())
            .split(area);

        draw_system_info_not_available(frame, chunks[0]);
        draw_log(frame, chunks[1], app);
    }
}

fn draw_system_info_not_available<B>(frame: &mut Frame<B>, area: Rect)
where
    B: Backend,
{
    let text = vec![Span::raw("System Info not available!")];
    frame.render_widget(
        Paragraph::new(Line::from(text))
            .block(Block::default().borders(Borders::ALL).title("System Info"))
            .wrap(Wrap { trim: true }),
        area,
    );
}

fn draw_uptime<B>(frame: &mut Frame<B>, area: Rect, uptime: u64)
where
    B: Backend,
{
    let remaining = uptime;
    let seconds = remaining % 60;
    let remaining = remaining / 60;
    let minutes = remaining % 60;
    let remaining = remaining / 60;
    let hours = remaining % 24;
    let days = remaining / 24;

    let text = vec![Span::raw(format!(
        "{} days, {} hours, {} minutes, {} seconds",
        days, hours, minutes, seconds
    ))];
    frame.render_widget(
        Paragraph::new(Line::from(text))
            .block(Block::default().borders(Borders::ALL).title("Uptime"))
            .wrap(Wrap { trim: true }),
        area,
    );
}

fn draw_cpu<B>(frame: &mut Frame<B>, area: Rect, cpu_load: &[f32])
where
    B: Backend,
{
    let chunks = Layout::default()
        .constraints(vec![Constraint::Length(1); cpu_load.len()])
        .margin(1)
        .split(area);

    frame.render_widget(
        Block::default().borders(Borders::ALL).title("CPU Load"),
        area,
    );

    for (index, load) in cpu_load.iter().enumerate() {
        let text = if index == 0 {
            Span::raw(" CPUs")
        } else {
            Span::raw(format!(" CPU{}", index))
        };
        draw_progress_bar_with_label(frame, chunks[index], *load as u16, text);
    }
}

fn draw_memory<B>(frame: &mut Frame<B>, area: Rect, mem: &info::Memory)
where
    B: Backend,
{
    let number_memory = if mem.swap == 0 { 1 } else { 2 };
    let chunks = Layout::default()
        .constraints(vec![Constraint::Length(1); number_memory])
        .margin(1)
        .split(area);

    frame.render_widget(
        Block::default()
            .borders(Borders::ALL)
            .title("Memory Utilization"),
        area,
    );

    let label = Span::raw(format!(
        " Memory [{}]",
        bytes_to_string_with_unit(mem.total)
    ));
    let memory_usage = (mem.used as f64 / mem.total as f64 * 100.0) as u16;
    draw_progress_bar_with_label(frame, chunks[0], memory_usage, label);

    if mem.swap > 0 {
        let label = Span::raw(format!(" Swap [{}]", bytes_to_string_with_unit(mem.swap)));
        let memory_usage = (mem.swap_used as f64 / mem.swap as f64 * 100.0) as u16;
        draw_progress_bar_with_label(frame, chunks[1], memory_usage, label);
    }
}

fn draw_progress_bar_with_label<B>(frame: &mut Frame<B>, area: Rect, percentage: u16, text: Span)
where
    B: Backend,
{
    let chunks = Layout::default()
        .constraints([Constraint::Percentage(60), Constraint::Min(0)].as_ref())
        .direction(Direction::Horizontal)
        .split(area);

    let gauge = Gauge::default()
        .gauge_style(
            Style::default()
                .fg(Color::Rgb(0xFF, 0xCC, 0x00))
                .bg(Color::Rgb(0x13, 0x13, 0x42))
                .add_modifier(Modifier::ITALIC),
        )
        .percent(percentage as u16);
    frame.render_widget(gauge, chunks[0]);

    frame.render_widget(Paragraph::new(Line::from(vec![text])), chunks[1]);
}

fn draw_network<B>(frame: &mut Frame<B>, area: Rect, net: &[info::Network])
where
    B: Backend,
{
    let mut text = Vec::<Line>::new();
    for network in net {
        text.push(Line::from(vec![Span::styled(
            format!("{}\n", network.interface_name),
            Style::default().add_modifier(Modifier::BOLD),
        )]));
        text.push(Line::from(vec![Span::raw(format!(
            "                       received: {}\n",
            network.received
        ))]));
        text.push(Line::from(vec![Span::raw(format!(
            "                    transmitted: {}\n",
            network.transmitted
        ))]));
        text.push(Line::from(vec![Span::raw(format!(
            "               packets_received: {}\n",
            network.packets_received
        ))]));
        text.push(Line::from(vec![Span::raw(format!(
            "            packets_transmitted: {}\n",
            network.packets_transmitted
        ))]));
        text.push(Line::from(vec![Span::raw(format!(
            "       total_errors_on_received: {}\n",
            network.total_errors_on_received
        ))]));
        text.push(Line::from(vec![Span::raw(format!(
            "    total_errors_on_transmitted: {}\n\n",
            network.total_errors_on_transmitted
        ))]));
    }

    frame.render_widget(
        Paragraph::new(text)
            .block(Block::default().borders(Borders::ALL).title("Network Load"))
            .wrap(Wrap { trim: false }),
        area,
    );
}

fn bytes_to_string_with_unit(bytes: u64) -> String {
    const GIG: u64 = 1024 * 1024 * 1024;
    const MEG: u64 = 1024 * 1024;
    const KILO: u64 = 1024;

    if bytes >= GIG {
        format!("{:.1} GiB", (bytes as f64) / GIG as f64)
    } else if bytes >= MEG {
        format!("{:.1} MiB", bytes as f64 / MEG as f64)
    } else if bytes >= KILO {
        format!("{:.1} KiB", bytes as f64 / KILO as f64)
    } else {
        format!("{} B", bytes)
    }
}

fn draw_log<B>(frame: &mut Frame<B>, area: Rect, app: &App)
where
    B: Backend,
{
    let title = vec![Span::styled(
        "Debug",
        Style::default()
            .fg(Color::Magenta)
            .add_modifier(Modifier::BOLD),
    )];
    let text = vec![Span::raw(app.debug.clone())];
    let paragraph = Paragraph::new(Line::from(text))
        .block(
            Block::default()
                .borders(Borders::ALL)
                .title(Line::from(title)),
        )
        .wrap(Wrap { trim: true });
    frame.render_widget(paragraph, area);
}
