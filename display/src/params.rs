use clap::Parser;

/// Larry Status Display
#[derive(Parser, Debug)]
#[command(author, version, about, long_about)]
pub struct Params {
    /// The update interal in  milliseonds
    #[arg(long, default_value = "1000")]
    pub update_interval: u64,
}
