mod params;

mod app;
use crate::app::App;

mod ui;

mod event;
use crate::event::{Config, Event, Events};

use std::io;
use std::time::Duration;

use clap::Parser;
use ratatui::backend::TermionBackend;
use ratatui::Terminal;
use termion::event::Key;
use termion::event::MouseEvent;
use termion::input::MouseTerminal;
use termion::raw::IntoRawMode;
use termion::screen::IntoAlternateScreen;

use std::error;
use std::fmt;
use std::result;

//TODO: move to error.rs
#[derive(Debug)]
enum Error {
    IoError(io::Error),
    RecvError(std::sync::mpsc::RecvError),
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error::IoError(err)
    }
}

impl From<std::sync::mpsc::RecvError> for Error {
    fn from(err: std::sync::mpsc::RecvError) -> Error {
        Error::RecvError(err)
    }
}

impl error::Error for Error {
    fn cause(&self) -> Option<&dyn error::Error> {
        Some(match *self {
            Error::IoError(ref err) => err as &dyn error::Error,
            Error::RecvError(ref err) => err as &dyn error::Error,
        })
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::IoError(ref err) => fmt::Display::fmt(err, f),
            Error::RecvError(ref err) => fmt::Display::fmt(err, f),
        }
    }
}

type Result<T> = result::Result<T, Error>;

fn main() -> Result<()> {
    larry_log::init("info", larry_log::Target::FileOnly);

    let params = params::Params::parse();
    log::debug!("{:?}", params);

    let events = Events::new(Config {
        tick_rate: Duration::from_millis(params.update_interval),
        ..Config::default()
    });

    let stdout = io::stdout().into_raw_mode()?;
    let stdout = MouseTerminal::from(stdout);
    let backend = TermionBackend::new(stdout.into_alternate_screen()?);
    let mut terminal = Terminal::new(backend)?;
    terminal.hide_cursor()?;

    let mut app = App::new("Larry Display Daemon");
    ui::draw(&mut terminal, &mut app, None)?;
    loop {
        let mut mouse_event: Option<MouseEvent> = None;
        match events.next()? {
            Event::Input(key) => {
                if let Key::Char(c) = key {
                    app.on_key(c);
                }
            }
            Event::Mouse(m) => {
                app.on_mouse(m);
                mouse_event = Some(m);
            }
            Event::Tick => {
                app.on_tick();
            }
        }
        if app.should_quit {
            break;
        }
        ui::draw(&mut terminal, &mut app, mouse_event)?;
    }

    Ok(())
}
