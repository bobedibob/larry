# Applications
- larry-camera-daemon
    - provides camera data from /dev/video0
    - listens on port 0x2323 for communication requests
    - libv4l-dev needs to be installed
- larry-display-daemon
    - displays system information on the attached touch display
- larry-drive-daemon
    - controls the I2C interface for the servo motors
    - listens on port 0xA2D2 for communication requests
- larry-gamepad-daemon
    - listen to events from `/dev/input/js0`
    - controls larry with a gamepad
        - analog pads for driving
        - button 9 (`Start`) to connect to larry-drive-daemon
        - button 8 and 0 (`Back` and `X`) to disconnet from larry-drive-daemon
- larry-http-daemon
    - provides a webinterface to control larry
    - listens on port 80 for incomming connections
    - document-root parameter must be set when starting the daemon
- larry-sysinfo-daemon
    - provides system information like CPU load, memory usage, etc
    - listens on port 0x6666 for communication requests

# Building

Build all applications
```
cargo build --all --release
```

Build single application
```
cargo build --bin larry-drive-daemon --release
```
