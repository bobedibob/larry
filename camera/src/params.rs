use clap::Parser;

use std::num::ParseIntError;
use std::str::FromStr;

/// Grabs image frames from the camera and transmits them via tcp (ETM protocol).
///
/// To get the parameter for your camera run "v4l2-ctl --list-formats-ext -d /dev/video0".
#[derive(Parser, Debug)]
#[command(author, version, about, long_about)]
pub struct Params {
    /// The device to use
    #[arg(short, long, default_value = "/dev/video0")]
    pub device: String,

    /// The mix of numerator and denominator. v4l2 uses frame intervals instead of frame rates
    #[arg(short, long, default_value = "1/30")]
    pub interval: Interval,

    /// Width and height of frame
    #[arg(short, long, default_value = "640x480")]
    pub resolution: Resolution,

    /// FourCC of format (e.g. `b"RGB3"`). Note that case matters
    #[arg(long, default_value = "MJPG")]
    pub format: String,

    /// Storage method of interlaced video. See `FIELD_*` constants
    /// https://linuxtv.org/downloads/v4l-dvb-apis/uapi/v4l/field-order.html
    /// [Options: Any, None, Top, Bottom, Interlaced, TopBottom, BottomTop, Alternate, InterlacedTopBottom, InterlacedBottomTop]
    #[arg(long, default_value = "None")]
    pub field: V4L2Field,

    /// Number of buffers in the queue of camera
    #[arg(short, long, default_value = "2")]
    pub buffers: u32,
}

#[derive(Parser, Debug, PartialEq, Clone)]
pub struct Interval {
    pub numerator: u32,
    pub denominator: u32,
}

impl Interval {
    pub fn as_tuple(&self) -> (u32, u32) {
        (self.numerator, self.denominator)
    }
}

impl FromStr for Interval {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let param: Vec<&str> = s.trim().split('/').collect();

        let numerator_fromstr = param[0].trim().parse::<u32>()?;
        let denominator_fromstr = param[1].trim().parse::<u32>()?;

        Ok(Interval {
            numerator: numerator_fromstr,
            denominator: denominator_fromstr,
        })
    }
}

#[derive(Parser, Debug, PartialEq, Clone)]
pub struct Resolution {
    pub width: u32,
    pub height: u32,
}

impl Resolution {
    pub fn as_tuple(&self) -> (u32, u32) {
        (self.width, self.height)
    }
}

impl FromStr for Resolution {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let param: Vec<&str> = s.trim().split('x').collect();

        let width_fromstr = param[0].trim().parse::<u32>()?;
        let height_fromstr = param[1].trim().parse::<u32>()?;

        Ok(Resolution {
            width: width_fromstr,
            height: height_fromstr,
        })
    }
}

#[derive(Parser, Copy, Clone, Debug)]
pub enum V4L2Field {
    Any,
    None,
    Top,
    Bottom,
    Interlaced,
    TopBottom,
    BottomTop,
    Alternate,
    InterlacedTopBottom,
    InterlacedBottomTop,
}

impl FromStr for V4L2Field {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let field = match s.trim() {
            "Any" => V4L2Field::Any,
            "None" => V4L2Field::None,
            "Top" => V4L2Field::Top,
            "Bottom" => V4L2Field::Bottom,
            "Interlaced" => V4L2Field::Interlaced,
            "TopBottom" => V4L2Field::TopBottom,
            "BottomTop" => V4L2Field::BottomTop,
            "Alternate" => V4L2Field::Alternate,
            "InterlacedTopBottom" => V4L2Field::InterlacedTopBottom,
            "InterlacedBottomTop" => V4L2Field::InterlacedBottomTop,
            _ => V4L2Field::Any,
        };

        Ok(field)
    }
}
