use etm::transport;
use etm::Service;
use rpc::camera::{
    CameraStream, Error, ProtocolVersion, Request, Response, SERVICE_CONNECTION_REQUEST_PORT,
    SERVICE_ID,
};

use bincode::config::Options;

use std::io::prelude::*;
use std::net::{Ipv4Addr, TcpStream};
use std::time;

type CameraConnection = etm::client::Connection<Request, Response, Error>;

const IP_ADDRESS: Ipv4Addr = Ipv4Addr::LOCALHOST;
// const IP_ADDRESS: Ipv4Addr = Ipv4Addr::new(10, 0, 0, 1);

fn main() {
    larry_log::init("info", larry_log::Target::DuplicateToConsole);

    if let Some(mut connection) =
        CameraConnection::new(IP_ADDRESS, SERVICE_CONNECTION_REQUEST_PORT, 1)
    {
        let service = Service::entity(SERVICE_ID.to_string(), ProtocolVersion::entity().version());
        if !connection.compatibility_check(service) {
            log::error!("Compatibility Check with camera daemon failed!");
        }

        if let Some(response) = connection.transceive(Request::Info()) {
            if let Response::Info(info) = response {
                log::info!("{}", info);
            }
        };

        if let Some(response) = connection.transceive(Request::StreamingPort()) {
            if let Response::StreamingPort { port } = response {
                log::info!("assigned streaming port: {}", port);

                if let Some(err) = TcpStream::connect((IP_ADDRESS, port))
                    .map(|mut stream| {
                        log::info!("streaming connection established");
                        stream.set_nodelay(true).expect("setting tcp no delay");
                        let read_timeout = Some(time::Duration::from_secs(10));
                        stream
                            .set_read_timeout(read_timeout)
                            .expect("setting tcp read timeout");

                        let serde = bincode::config::DefaultOptions::new()
                            .with_big_endian()
                            .with_fixint_encoding();

                        let mut datalengthbuffer = [0u8; 8];

                        loop {
                            let mut databuffer = Vec::<u8>::new();
                            if let Some(err) = &mut stream.read_exact(&mut datalengthbuffer).err() {
                                panic!("error reading buffer: {:?}", err);
                            }
                            let bytes_to_read = u64::from_be_bytes(datalengthbuffer);
                            log::info!("received image with {} bytes", bytes_to_read);
                            if let Some(err) = stream
                                .try_clone()
                                .expect("reading image data")
                                .take(bytes_to_read)
                                .read_to_end(&mut databuffer)
                                .err()
                            {
                                panic!("didn't read enough: {:?}", err);
                            }

                            let response = serde
                                .deserialize::<transport::Transmission<CameraStream>>(&databuffer);
                            if let Ok(transmission) = response {
                                if let transport::Type::Stream::<CameraStream>(camera_stream) =
                                    transmission.r#type
                                {
                                    log::info!(
                                        "frame number: {}; timestamp: {}",
                                        transmission.id,
                                        camera_stream.timestamp
                                    )
                                }
                            }
                        }
                    })
                    .err()
                {
                    log::error!("could not connect to assigned streaming port: {:?}", err);
                }
            }
        };
    }
}
