mod params;

use etm::server::{MessageProcessing, Server};
use etm::transport;
use etm::Service;
use rpc::camera::{
    CameraStream, ProtocolVersion, Request, Response, SERVICE_CONNECTION_REQUEST_PORT, SERVICE_ID,
};

use bincode::config::Options;
use clap::Parser;
use rscam;

use std::io::prelude::*;
use std::net::{Ipv4Addr, TcpListener};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{Arc, Mutex};
use std::thread;

struct LarryCamera {
    stream_running: Arc<AtomicBool>,
    streamer: Mutex<Option<thread::JoinHandle<()>>>,
    params: Arc<params::Params>,
}

impl LarryCamera {
    fn new() -> Self {
        let params = Arc::new(params::Params::parse());
        log::debug!("{:?}", params);

        LarryCamera {
            stream_running: Arc::new(AtomicBool::new(false)),
            streamer: Mutex::new(None),
            params,
        }
    }

    fn open_stream(&self) -> u16 {
        self.stop_stream();
        // get listener
        let listener = TcpListener::bind((Ipv4Addr::UNSPECIFIED, 0)).expect("weiting for client");
        let local_port: u16 = listener.local_addr().expect("local port").port();
        log::debug!("waiting on port {}", local_port);

        let format = self.params.format.clone();

        let camera_config = rscam::Config {
            interval: self.params.interval.as_tuple(),
            resolution: self.params.resolution.as_tuple(),
            format: b"MJPG",
            field: self.params.field as u32,
            nbuffers: self.params.buffers,
        };

        let running = self.stream_running.clone();
        running.store(true, Ordering::Relaxed);
        let mut streamer = self.streamer.lock().expect("getting lock");
        let params = self.params.clone();
        *streamer = Some(thread::spawn(move || {
            if let Ok(mut stream) =
                etm::listener_accept_nonblocking(listener, std::time::Duration::from_secs(2))
            {
                if let Some(err) = stream.set_nodelay(true).err() {
                    log::error!("couldn't set tcp nodelay: {:?}", err);
                }

                let mut camera_config = camera_config;
                let mut camera = rscam::new(&params.device)
                    .expect(&format!("new video device on {}", params.device));
                camera_config.format = format.as_bytes();

                log::info!(
                    "{:?}, {:?}, {:?}, {}, {}",
                    camera_config.interval,
                    camera_config.resolution,
                    camera_config.format,
                    camera_config.field,
                    camera_config.nbuffers
                );
                camera.start(&camera_config).expect("running the camera");

                let serde = bincode::config::DefaultOptions::new()
                    .with_big_endian()
                    .with_fixint_encoding();

                let mut sequence_number = 0u64;
                while running.load(Ordering::Relaxed) {
                    let frame = camera.capture().expect("capturing video");

                    let frame_data = frame.to_vec();
                    let camera_stream = CameraStream {
                        timestamp: frame.get_timestamp(),
                        frame: &frame_data,
                    };
                    let transmission = transport::Transmission {
                        id: sequence_number,
                        r#type: transport::Type::Stream(camera_stream),
                    };
                    sequence_number += 1;

                    let data = serde.serialize(&transmission).unwrap();
                    let mut senddata = (data.len() as u64).to_be_bytes().to_vec();
                    senddata.extend(data);

                    if let Err(err) = stream.write(&senddata) {
                        log::error!("{:?}", err);
                        break;
                    }
                }

                if let Some(err) = camera.stop().err() {
                    log::error!("stopping camera: {:?}", err);
                }
            }
        }));

        local_port
    }

    fn stop_stream(&self) {
        self.stream_running.store(false, Ordering::Relaxed);
        if let Some(th) = self.streamer.lock().expect("getting lock").take() {
            if let Err(e) = th.join() {
                log::error!("{:?}", e);
            }
        };
    }
}

impl Drop for LarryCamera {
    fn drop(&mut self) {
        self.stop_stream();
    }
}

impl MessageProcessing for LarryCamera {
    type Rq = rpc::camera::Request;
    type Rsp = rpc::camera::Response;
    type E = rpc::camera::Error;

    fn new() -> Arc<Self> {
        Arc::new(LarryCamera::new())
    }

    fn execute(&self, _connection_id: u32, rpc: Self::Rq) -> Result<Self::Rsp, Self::E> {
        match rpc {
            Request::Ping() => Ok(Response::Pong()),
            Request::Info() => {
                let version_firmware =
                    env!("CARGO_PKG_NAME").to_string() + " - v" + env!("CARGO_PKG_VERSION");

                Ok(Response::Info(version_firmware))
            }
            Request::StreamingPort() => {
                let port = self.open_stream();

                Ok(Response::StreamingPort { port })
            }
        }
    }
}

fn main() -> std::io::Result<()> {
    larry_log::init("info", larry_log::Target::DuplicateToConsole);

    let server = Server::<LarryCamera>::new(
        SERVICE_CONNECTION_REQUEST_PORT,
        Service::entity(SERVICE_ID.to_string(), ProtocolVersion::entity().version()),
    );
    server.run()?;

    Ok(())
}
