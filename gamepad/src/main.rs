mod params;

use rpc::drive;

type Connection = etm::client::Connection<drive::Request, drive::Response, drive::Error>;

use clap::Parser;

use std::cmp::PartialEq;
use std::fs::File;
use std::io::BufReader;
use std::io::Read;
use std::net::Ipv4Addr;
use std::path::PathBuf;
use std::str::FromStr;

#[derive(Debug, PartialEq)]
#[repr(u8)]
enum JoystickEventType {
    Real,
    Synthetic,
}

#[derive(Debug)]
#[repr(u8)]
enum JoystickInputType {
    Button = 1,
    Axis,
}

#[derive(Debug)]
struct JoystickEvent {
    timestamp: u32,
    value: i16,
    event_type: JoystickEventType,
    input_type: JoystickInputType,
    input_number: u8,
}

#[derive(Debug, Default)]
struct Pilot {
    motor_driver: Option<Box<Connection>>,
    move_x: i16,
    move_y: i16,
    rotate: i16,
}

impl Pilot {
    fn handle_event(&mut self, event: JoystickEvent) {
        if event.event_type != JoystickEventType::Real {
            return;
        }

        match event.input_type {
            JoystickInputType::Button if self.is_start_button(event.input_number) => {
                if event.value == 1 {
                    self.start_motor();
                }
            }
            JoystickInputType::Button if self.is_stop_button(event.input_number) => {
                if event.value == 1 {
                    self.stop_motor();
                }
            }
            JoystickInputType::Axis => {
                self.drive(&event);
            }
            _ => log::debug!("Invalid event: {:?}", event),
        }
    }

    fn is_start_button(&self, input_number: u8) -> bool {
        const START_BUTTON: u8 = 9;
        input_number == START_BUTTON
    }

    fn is_stop_button(&self, input_number: u8) -> bool {
        const X_BUTTON: u8 = 0;
        const STOP_BUTTON: u8 = 9;
        input_number == X_BUTTON || input_number == STOP_BUTTON
    }

    fn start_motor(&mut self) {
        log::info!("Start Motor");
        self.motor_driver = Connection::new(
            Ipv4Addr::LOCALHOST,
            drive::SERVICE_CONNECTION_REQUEST_PORT,
            1,
        );

        if let Some(connection) = self.motor_driver.as_ref() {
            let service = etm::Service::entity(
                drive::SERVICE_ID.to_string(),
                drive::ProtocolVersion::entity().version(),
            );

            if !connection.compatibility_check(service) {
                log::error!("Compatibility Check with drive daemon failed!");
            }
        } else {
            log::error!("Could not open connection");
        }
    }

    fn stop_motor(&mut self) {
        log::info!("Stop Motor");

        self.move_x = 0;
        self.move_y = 0;
        self.rotate = 0;

        self.update_movement();

        self.motor_driver = None;
    }

    fn drive(&mut self, event: &JoystickEvent) {
        match event.input_number {
            0 => self.move_x = event.value,
            1 => self.move_y = event.value,
            2 => self.rotate = event.value,
            _ => return,
        }

        self.update_movement()
    }

    fn update_movement(&mut self) {
        if let Some(motor_driver) = self.motor_driver.as_mut() {
            let mut speed = ((self.move_x as f64).powi(2) + (self.move_y as f64).powi(2)).sqrt()
                / i16::MAX as f64
                * 100.;
            if speed > 99.5 {
                speed = 100.;
            } else if speed < -99.5 {
                speed = -100.;
            }

            let direction = if self.move_x == 0 && self.move_y == 0 {
                0_f64
            } else {
                (self.move_x as f64).atan2(-(self.move_y as f64)) / std::f64::consts::PI * 180.0
            };
            let rotation_speed = self.rotate as f64 / i16::MAX as f64 * 100.;

            if motor_driver
                .transceive(drive::Request::Move {
                    speed,
                    direction,
                    rotation_speed,
                })
                .is_none()
            {
                log::error!("could not send speed to drive daemon");
            }
        }
    }
}

fn open_device(path: &PathBuf) -> std::io::Result<BufReader<File>> {
    let fd = File::open(path).map_err(|err| {
        log::debug!("Could not open input device: {:?}", err);
        err
    })?;

    Ok(BufReader::new(fd))
}

fn get_event(reader: &mut BufReader<File>) -> std::io::Result<JoystickEvent> {
    let mut ev_buf = [0u8; 8];
    reader.read_exact(&mut ev_buf)?;

    Ok(JoystickEvent {
        // https://www.kernel.org/doc/Documentation/input/joystick-api.txt
        timestamp: u32::from_le_bytes([ev_buf[0], ev_buf[1], ev_buf[2], ev_buf[3]]),
        value: i16::from_le_bytes([ev_buf[4], ev_buf[5]]),
        event_type: if ev_buf[6] & 0x80 == 0x80 {
            JoystickEventType::Synthetic
        } else {
            JoystickEventType::Real
        },
        input_type: if ev_buf[6] & 0x03 == 2 {
            JoystickInputType::Axis
        } else {
            JoystickInputType::Button
        },
        input_number: ev_buf[7],
    })
}

fn event_loop(mut reader: BufReader<File>) {
    let mut pilot: Pilot = Default::default();

    while let Ok(event) = get_event(&mut reader).map_err(|e| {
        log::error!("Could not read event! {:?}", e);
        e
    }) {
        pilot.handle_event(event);
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    larry_log::init("info", larry_log::Target::DuplicateToConsole);

    let params = params::Params::parse();
    log::debug!("{:?}", params);

    let path = PathBuf::from_str("/dev/input/js0")?;

    loop {
        match open_device(&path) {
            Ok(reader) => event_loop(reader),
            Err(_) => std::thread::sleep(std::time::Duration::from_secs(1)),
        }
    }
}
