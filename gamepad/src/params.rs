use clap::Parser;

/// Gamepad control for larry.
#[derive(Parser, Debug)]
#[command(author, version, about, long_about)]
pub struct Params {}
