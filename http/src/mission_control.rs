use rpc::camera;
use rpc::drive;
use rpc::system::info;

use anyhow::{anyhow, Result};
use bincode::config::Options;

use std::collections::HashMap;
use std::io::prelude::*;
use std::net::SocketAddr;
use std::net::{Ipv4Addr, TcpStream};
use std::sync::atomic::AtomicBool;
use std::sync::{Arc, Mutex};
use std::thread::JoinHandle;
use std::time;

type DriveConnection = etm::client::Connection<drive::Request, drive::Response, drive::Error>;
type SysInfoConnection = etm::client::Connection<info::Request, info::Response, info::Error>;
type CameraConnection = etm::client::Connection<camera::Request, camera::Response, camera::Error>;

struct ImageWithTimestamp {
    timestamp: u64,
    frame: Vec<u8>,
}

pub struct LarryMissionControl {
    drive_connection: Arc<Mutex<Option<Box<DriveConnection>>>>,
    sysinfo_connection: Mutex<Option<Box<SysInfoConnection>>>,
    camera_connection: Mutex<Option<Box<CameraConnection>>>,
    camera_stream_thread_join_handle: Mutex<Option<JoinHandle<()>>>,
    run_camera_stream: Arc<AtomicBool>,
    latest_image: Arc<Mutex<Option<ImageWithTimestamp>>>,
    image_delivery_lookup: Mutex<HashMap<SocketAddr, u64>>,
    drive_timestamp_last_command: Arc<Mutex<time::Instant>>,
    drive_watchdog: Mutex<Option<JoinHandle<()>>>,
    run_drive_vatchdog: Arc<AtomicBool>,
}

impl Drop for LarryMissionControl {
    fn drop(&mut self) {
        let _ = self.disconnect_all();
    }
}

impl LarryMissionControl {
    pub fn new() -> Self {
        LarryMissionControl {
            drive_connection: Arc::new(Mutex::new(None)),
            sysinfo_connection: Mutex::new(None),
            camera_connection: Mutex::new(None),
            camera_stream_thread_join_handle: Mutex::new(None),
            run_camera_stream: Arc::new(AtomicBool::new(false)),
            latest_image: Arc::new(Mutex::new(None)),
            image_delivery_lookup: Mutex::new(HashMap::new()),
            drive_timestamp_last_command: Arc::new(Mutex::new(time::Instant::now())),
            drive_watchdog: Mutex::new(None),
            run_drive_vatchdog: Arc::new(AtomicBool::new(false)),
        }
    }

    pub fn run(self, port: u16) {
        let _ = self.connect_all().map_err(|err| log::error!("{:?}", err));

        let active_remote_addr: Arc<Mutex<Option<SocketAddr>>> = Arc::new(Mutex::new(None));

        #[allow(clippy::cognitive_complexity)]
        rouille::start_server(format!("0.0.0.0:{}", port), move |request| {
            router!(request,
                (GET) (/) => {
                    let request = rouille::Request::fake_https("GET", "/mission-control.html", vec![], vec![]);
                    rouille::match_assets(&request, ".")
                },

                (GET) (/control/drive/move/{speed: f64}/{direction: f64}/{rotation_speed: f64}) => {
                    active_remote_addr.lock().expect("Obtaining active_remote_ip lock").as_ref().map(|active_remote_addr| {
                        if request.remote_addr() == active_remote_addr {
                            let _ = self.set_movement(speed, direction, rotation_speed).map_err(|err| log::error!("set speed error: {:?}", err));
                        }
                    });

                    rouille::Response::empty_204()
                },

                (GET) (/re-init) => {
                    let _ = self.disconnect_all().map_err(|err| log::error!("re-init (diconnect_all): {:?}", err));
                    let _ = self.connect_all().map_err(|err| log::error!("re-init (connect_all): {:?}", err));
                    *active_remote_addr.lock().expect("Obtaining active_remote_ip lock") = Some(*request.remote_addr());
                    rouille::Response::empty_204()
                },

                (GET) (/control/drive/stop) => {
                    active_remote_addr.lock().expect("Obtaining active_remote_ip lock").as_ref().map(|active_remote_addr| {
                        if request.remote_addr() == active_remote_addr {
                            let _ = self.stop_movement().map_err(|err| log::error!("set stop error: {:?}", err));
                        }
                    });

                    rouille::Response::empty_204()
                },

                (GET) (/system/info) => {
                    let request = rouille::Request::fake_http("GET", "/system-info.html", vec![], vec![]);
                    rouille::match_assets(&request, ".")
                },

                (GET) (/system/info/all) => {
                    if let Some(sysinfo) = self.get_all_sysinfo() {
                        rouille::Response::json(&sysinfo)
                    } else {
                        rouille::Response::empty_404()
                    }
                },

                (GET) (/camera) => {
                    let request = rouille::Request::fake_http("GET", "/camera.html", vec![], vec![]);
                    rouille::match_assets(&request, ".")
                },

                (GET) (/camera/image) => {
                    let _ = self.connect_camera().map_err(|err| log::error!("{:?}", err));

                    let remote_addr = request.remote_addr();

                    if let Some(image) = self.latest_image.lock().expect("Obtaining latest_image lock").as_ref() {
                        let timestamp_last_delivery = self.image_delivery_lookup.lock().expect("Obtaining image_delivery_lookup lock")
                                                            .insert(*remote_addr, image.timestamp).unwrap_or(image.timestamp.saturating_sub(1));
                        if image.timestamp > timestamp_last_delivery {
                            rouille::Response::from_data("application/octet-stream", image.frame.clone())
                        } else {
                            rouille::Response::empty_204()
                        }
                    } else {
                        rouille::Response::empty_204()
                    }
                },

                _ => {
                    log::error!("unknown request: {:?}", request);
                    rouille::Response::empty_404()
                }
            )
        });
    }

    fn connect_all(&self) -> Result<()> {
        self.connect_drive()?;
        self.connect_sysinfo()?;
        self.connect_camera()?;

        Ok(())
    }

    fn disconnect_all(&self) -> Result<()> {
        self.run_camera_stream
            .store(false, std::sync::atomic::Ordering::Relaxed);
        self.camera_stream_thread_join_handle
            .lock()
            .expect("Get lock")
            .take()
            .map(|join_handle| join_handle.join());

        self.run_drive_vatchdog
            .store(false, std::sync::atomic::Ordering::Relaxed);
        self.drive_watchdog
            .lock()
            .expect("Get lock")
            .take()
            .map(|join_handle| join_handle.join());

        self.camera_connection.lock().expect("Get lock").take();
        self.sysinfo_connection.lock().expect("Get lock").take();
        self.drive_connection.lock().expect("Get lock").take();

        Ok(())
    }

    fn connect_drive(&self) -> Result<()> {
        let mut connection = self
            .drive_connection
            .lock()
            .map_err(|err| anyhow!("Locking failed: {:?}", err))?;

        if connection.is_some() {
            return Ok(());
        }

        let new_connection = DriveConnection::new(
            Ipv4Addr::LOCALHOST,
            drive::SERVICE_CONNECTION_REQUEST_PORT,
            1,
        )
        .ok_or(anyhow!("Could not create drive connection"))?;

        let service = etm::Service::entity(
            drive::SERVICE_ID.to_string(),
            drive::ProtocolVersion::entity().version(),
        );

        if new_connection.compatibility_check(service) == false {
            return Err(anyhow!("Compatibility Check with drive daemon failed!"));
        }

        // set timestamp before storing connection in order to not leave a valid connection in self when the mutex locking fails
        let mut timestamp = self
            .drive_timestamp_last_command
            .lock()
            .map_err(|err| anyhow!("Locking failed: {:?}", err))?;
        *timestamp = time::Instant::now();

        *connection = Some(new_connection);

        self.run_drive_vatchdog
            .store(true, std::sync::atomic::Ordering::Relaxed);

        let drive_connection = self.drive_connection.clone();
        let run_drive_vatchdog = self.run_drive_vatchdog.clone();
        let drive_timestamp_last_command = self.drive_timestamp_last_command.clone();

        let th = std::thread::spawn(move || {
            Self::drive_watchdog(
                drive_connection,
                run_drive_vatchdog,
                drive_timestamp_last_command,
            );
        });
        let mut join_handle = self.drive_watchdog.lock().expect("Locking failed");
        *join_handle = Some(th);

        Ok(())
    }

    fn connect_sysinfo(&self) -> Result<()> {
        let mut connection = self
            .sysinfo_connection
            .lock()
            .map_err(|err| anyhow!("Locking failed: {:?}", err))?;

        if connection.is_some() {
            return Ok(());
        }

        let new_connection = SysInfoConnection::new(
            Ipv4Addr::LOCALHOST,
            info::SERVICE_CONNECTION_REQUEST_PORT,
            1,
        )
        .ok_or(anyhow!("Could not create system info connection"))?;

        let service = etm::Service::entity(
            info::SERVICE_ID.to_string(),
            info::ProtocolVersion::entity().version(),
        );

        if new_connection.compatibility_check(service) == false {
            return Err(anyhow!(
                "Compatibility Check with system info daemon failed!"
            ));
        }
        *connection = Some(new_connection);

        Ok(())
    }

    fn connect_camera(&self) -> Result<()> {
        let mut connection = self
            .camera_connection
            .lock()
            .map_err(|err| anyhow!("Locking failed: {:?}", err))?;

        if connection.is_some() {
            return Ok(());
        }

        let new_connection = CameraConnection::new(
            Ipv4Addr::LOCALHOST,
            camera::SERVICE_CONNECTION_REQUEST_PORT,
            1,
        )
        .ok_or(anyhow!("Could not create camera connection"))?;

        let service = etm::Service::entity(
            camera::SERVICE_ID.to_string(),
            camera::ProtocolVersion::entity().version(),
        );

        if new_connection.compatibility_check(service) == false {
            return Err(anyhow!("Compatibility Check with camera daemon failed!"));
        }
        *connection = Some(new_connection);
        let connection = connection.as_deref_mut().unwrap();

        let response = connection
            .transceive(camera::Request::StreamingPort())
            .ok_or(anyhow!("Could not open port for camera stream!"))?;

        if let camera::Response::StreamingPort { port } = response {
            TcpStream::connect((Ipv4Addr::LOCALHOST, port))
                .map(|stream| {
                    let run_camera_stream = self.run_camera_stream.clone();
                    let latest_image = self.latest_image.clone();

                    run_camera_stream.store(true, std::sync::atomic::Ordering::Relaxed);

                    let th = std::thread::spawn(move || {
                        Self::capture_camera_stream(stream, run_camera_stream, latest_image);
                    });
                    let mut join_handle = self
                        .camera_stream_thread_join_handle
                        .lock()
                        .expect("Locking failed");
                    *join_handle = Some(th);
                })
                .map_err(|err| anyhow!("could not connect to assigned streaming port: {:?}", err))?
        }

        Ok(())
    }

    fn capture_camera_stream(
        mut stream: TcpStream,
        run_camera_stream: Arc<AtomicBool>,
        latest_image: Arc<Mutex<Option<ImageWithTimestamp>>>,
    ) {
        stream.set_nodelay(true).expect("setting tcp no delay");
        let read_timeout = Some(time::Duration::from_secs(10));
        stream
            .set_read_timeout(read_timeout)
            .expect("setting tcp read timeout");

        let serde = bincode::config::DefaultOptions::new()
            .with_big_endian()
            .with_fixint_encoding();

        let mut datalengthbuffer = [0u8; 8];

        while run_camera_stream.load(std::sync::atomic::Ordering::Relaxed) {
            let mut databuffer = Vec::<u8>::new();
            if let Some(err) = &mut stream.read_exact(&mut datalengthbuffer).err() {
                // panic!("error reading buffer: {:?}", err);
                match err.kind() {
                    std::io::ErrorKind::UnexpectedEof => break,
                    _ => {
                        log::error!("error reading buffer: {:?}", err)
                    }
                };
            }

            let bytes_to_read = u64::from_be_bytes(datalengthbuffer);
            if let Some(err) = stream
                .try_clone()
                .expect("reading image data")
                .take(bytes_to_read)
                .read_to_end(&mut databuffer)
                .err()
            {
                log::error!("didn't read enough: {:?}", err);
                continue;
            }

            let response = serde
                .deserialize::<etm::transport::Transmission<camera::CameraStream>>(&databuffer);
            if let Ok(transmission) = response {
                if let etm::transport::Type::Stream::<camera::CameraStream>(camera_stream) =
                    transmission.r#type
                {
                    let mut image = latest_image.lock().expect("Obtaining latest_image lock");
                    *image = Some(ImageWithTimestamp {
                        timestamp: camera_stream.timestamp,
                        frame: camera_stream.frame.to_owned(),
                    });
                }
            }
        }
    }

    fn drive_watchdog(
        drive_connection: Arc<Mutex<Option<Box<DriveConnection>>>>,
        run_drive_watchdog: Arc<AtomicBool>,
        drive_timestamp_last_command: Arc<Mutex<time::Instant>>,
    ) {
        let timestamp_watchdog_start = time::Instant::now();
        while run_drive_watchdog.load(std::sync::atomic::Ordering::Relaxed) {
            let timestamp_last_command = if let Ok(t) = drive_timestamp_last_command.lock() {
                *t
            } else {
                timestamp_watchdog_start
            };

            if time::Instant::now().duration_since(timestamp_last_command)
                > time::Duration::from_secs(1)
            {
                let _ = Self::stop_movement_no_connect(&drive_connection);
            }

            let delay = time::Instant::now();
            while run_drive_watchdog.load(std::sync::atomic::Ordering::Relaxed)
                && delay.elapsed() < time::Duration::from_millis(500)
            {
                std::thread::sleep(std::time::Duration::from_millis(10));
            }
        }
    }

    fn set_movement(&self, speed: f64, direction: f64, rotation_speed: f64) -> Result<()> {
        self.connect_drive()?;
        let mut connection = self
            .drive_connection
            .lock()
            .map_err(|err| anyhow!("Locking failed: {:?}", err))?;

        if let Some(connection) = connection.as_deref_mut() {
            connection
                .transceive(drive::Request::Move {
                    speed,
                    direction,
                    rotation_speed,
                })
                .ok_or(anyhow!("could not send speed to drive daemon"))?;
            let mut timestamp = self
                .drive_timestamp_last_command
                .lock()
                .map_err(|err| anyhow!("Locking failed: {:?}", err))?;
            *timestamp = time::Instant::now();
        }

        Ok(())
    }

    fn stop_movement(&self) -> Result<()> {
        self.connect_drive()?;
        Self::stop_movement_no_connect(&self.drive_connection)
    }

    fn stop_movement_no_connect(
        drive_connection: &Mutex<Option<Box<DriveConnection>>>,
    ) -> Result<()> {
        let mut connection = drive_connection
            .lock()
            .map_err(|err| anyhow!("Locking failed: {:?}", err))?;

        if let Some(connection) = connection.as_deref_mut() {
            connection
                .transceive(drive::Request::Stop())
                .ok_or(anyhow!("could not send stop signal to drive daemon"))?;
        }

        Ok(())
    }

    fn get_all_sysinfo(&self) -> Option<info::AllSystemInfo> {
        let _ = self
            .connect_sysinfo()
            .map_err(|err| log::error!("{:?}", err))
            .ok()?;

        let mut connection = self.sysinfo_connection.lock().ok()?;
        let response = connection
            .as_mut()?
            .transceive(info::Request::AllSystemInfo());
        if let info::Response::AllSystemInfo(sysinfo) = response? {
            Some(sysinfo)
        } else {
            None
        }
    }
}
