use clap::Parser;

use std::path::PathBuf;

/// Larry Mission Control
#[derive(Parser, Debug)]
#[command(author, version, about, long_about)]
pub struct Params {
    /// The port for the http server
    #[arg(long, default_value = "80")]
    pub http_port: u16,

    #[arg(long)]
    pub document_root: PathBuf,
}
