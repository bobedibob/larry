mod params;

mod mission_control;
use crate::mission_control::LarryMissionControl;

use clap::Parser;

use std::env;

#[macro_use]
extern crate rouille;

fn main() {
    larry_log::init("info", larry_log::Target::DuplicateToConsole);

    let params = params::Params::parse();
    log::debug!("{:?}", params);

    env::set_current_dir(&params.document_root).expect("path to document-root");

    let server = LarryMissionControl::new();
    server.run(params.http_port);
}
