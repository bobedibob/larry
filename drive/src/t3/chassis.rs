use super::ecu::{Ecu, Motor};
use super::wheel::Wheel;

use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug, PartialEq, Default)]
pub(super) struct Movement {
    pub(super) speed: f64,
    pub(super) direction: f64,
    pub(super) rotation_speed: f64,
}

#[derive(Debug)]
pub(super) struct Chassis {
    front_left: Wheel,
    front_right: Wheel,
    rear_left: Wheel,
    rear_right: Wheel,
    current_movement: Movement,
}

impl Chassis {
    pub(super) fn new() -> Option<Self> {
        let front_ecu = Rc::new(RefCell::new(Ecu::new(0x40)?));
        let rear_ecu = Rc::new(RefCell::new(Ecu::new(0x41)?));

        let front_left = Wheel::new(Some(front_ecu.clone()), Motor::B);
        let front_right = Wheel::new(Some(front_ecu.clone()), Motor::A);
        let rear_left = Wheel::new(Some(rear_ecu.clone()), Motor::A);
        let rear_right = Wheel::new(Some(rear_ecu.clone()), Motor::B);

        Some(Self {
            front_left,
            front_right,
            rear_left,
            rear_right,
            current_movement: Default::default(),
        })
    }

    pub(super) fn run(&mut self, movement: Movement) {
        if movement != self.current_movement {
            let direction_frontal = movement.direction.to_radians().cos();
            let direction_lateral = movement.direction.to_radians().sin();

            let frontal_speed = movement.speed * direction_frontal;
            let frontal_speed_max = (100.0 * direction_frontal).abs();

            let lateral_speed = movement.speed * direction_lateral;
            let lateral_speed_max = (100.0 * direction_lateral).abs();

            let normalize_factor = 100.0 / (frontal_speed_max + lateral_speed_max);

            let front_left =
                (frontal_speed + lateral_speed) * normalize_factor - movement.rotation_speed;
            let front_right =
                (frontal_speed - lateral_speed) * normalize_factor + movement.rotation_speed;
            let rear_left =
                (frontal_speed - lateral_speed) * normalize_factor - movement.rotation_speed;
            let rear_right =
                (frontal_speed + lateral_speed) * normalize_factor + movement.rotation_speed;

            let max_speed = [
                front_left.abs(),
                front_right.abs(),
                rear_left.abs(),
                rear_right.abs(),
            ]
            .iter()
            .fold(0.0_f64, |max_speed, &speed| max_speed.max(speed));

            if max_speed < 100.0 {
                self.front_left.run(front_left);
                self.front_right.run(front_right);
                self.rear_left.run(rear_left);
                self.rear_right.run(rear_right);
            } else {
                let normalize_factor = 100.0 / max_speed;
                self.front_left.run(front_left * normalize_factor);
                self.front_right.run(front_right * normalize_factor);
                self.rear_left.run(rear_left * normalize_factor);
                self.rear_right.run(rear_right * normalize_factor);
            };

            self.current_movement = movement;
        }
    }

    pub(super) fn stop(&mut self) {
        self.front_left.stop();
        self.front_right.stop();
        self.rear_left.stop();
        self.rear_right.stop();
        self.current_movement = Default::default();
    }
}
