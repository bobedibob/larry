use super::chassis::{Chassis, Movement};
use crate::vehicle::Vehicle;

use std::sync::mpsc;
use std::thread;

#[derive(Debug)]
enum EngineCmd {
    PowerOff,
    Stop,
    Run(Movement),
}

type EngineRequest = mpsc::Sender<EngineCmd>;
type EngineRequestInbox = mpsc::Receiver<EngineCmd>;

#[derive(Debug)]
pub struct T3Vehicle {
    engine_control: Option<thread::JoinHandle<()>>,
    engine_request: EngineRequest,
}

impl T3Vehicle {
    pub fn new() -> Option<Self> {
        let (engine_request, request_inbox) = mpsc::channel();

        let mut vehicle = Self {
            engine_control: None,
            engine_request,
        };
        vehicle.stop();
        vehicle.run_engine_control(request_inbox);
        Some(vehicle)
    }

    fn run_engine_control(&mut self, inbox: EngineRequestInbox) {
        self.engine_control = Some(thread::spawn(move || {
            if let Some(mut chassis) = Chassis::new() {
                while let Ok(mut engine_cmd) = inbox.recv() {
                    // we are only interested in the newest request if the pipeline is not empty
                    while let Ok(newest_engine_cmd) = inbox.try_recv() {
                        engine_cmd = newest_engine_cmd;
                    }

                    match engine_cmd {
                        EngineCmd::PowerOff => break,
                        EngineCmd::Stop => chassis.stop(),
                        EngineCmd::Run(movement) => chassis.run(movement),
                    }
                }
            }
        }));
    }
}

impl Drop for T3Vehicle {
    fn drop(&mut self) {
        if let Err(e) = self.engine_request.send(EngineCmd::PowerOff) {
            log::error!("{}", e);
        }
        if let Some(th) = self.engine_control.take() {
            if let Err(e) = th.join() {
                log::error!("{:?}", e);
            }
        };
    }
}

impl Vehicle for T3Vehicle {
    fn run(&mut self, speed: f64, direction: f64, rotation_speed: f64) {
        let movement = Movement {
            speed,
            direction,
            rotation_speed,
        };

        if let Err(e) = self.engine_request.send(EngineCmd::Run(movement)) {
            log::error!("{}", e);
        }
    }

    fn stop(&mut self) {
        if let Err(e) = self.engine_request.send(EngineCmd::Stop) {
            log::error!("{}", e);
        }
    }
}
