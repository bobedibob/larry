use rppal::i2c::{I2c, Result};

use std::thread;
use std::time::Duration;

#[derive(Debug, Copy, Clone)]
#[repr(u8)]
pub(super) enum Motor {
    A,
    B,
}

#[derive(Debug)]
pub(super) struct Ecu {
    bus: I2c,
    i2c_address: u16,
}

#[derive(Debug)]
#[repr(u8)]
enum Register {
    Mode1 = 0x00,
    DriverBase = 0x06,
    Prescale = 0xFE,
}

impl Ecu {
    pub(super) fn new(i2c_address: u16) -> Option<Self> {
        let mut bus = I2c::new().ok()?;

        bus.set_slave_address(i2c_address).ok()?;
        bus.set_timeout(10).ok()?;

        Self::init_ecu(&mut bus).ok()?;

        Some(Self { bus, i2c_address })
    }

    fn init_ecu(bus: &mut I2c) -> Result<()> {
        const OSC_CLOCK: u32 = 25_000_000;
        const MAX_RESOLUTION: u32 = 4096;
        // NOTE if the frequency becomes a parameter, check if freq is in the correct range
        // const MIN_PWM_FREQ: u32 = 24;
        // const MAX_PWM_FREQ: u32 = 1526;
        let freq = 1000_u32;
        let prescaler = (OSC_CLOCK / (MAX_RESOLUTION * freq) - 1) as u8;

        // reset mode1 register
        bus.write(&[Register::Mode1 as u8, 0x00])?;

        // got to sleep
        bus.write(&[Register::Mode1 as u8, 0x10])?;

        // set prescaler
        bus.write(&[Register::Prescale as u8, prescaler])?;

        // restore old mode from before sleep
        bus.write(&[Register::Mode1 as u8, 0x00])?;

        thread::sleep(Duration::from_millis(5)); // delay for processing

        // turn on auto increment to be able to write to more than one register with a datagram
        bus.write(&[Register::Mode1 as u8, 0x20])?;

        Ok(())
    }

    pub(super) fn set_speed(&mut self, motor: Motor, speed: i8) -> Result<()> {
        // brief description of the PCA9685 driver registers
        // - Register::DriverBase is the address for the first driver
        // - 4 bytes per driver [ON_LOW, ON_HIGH, OFF_LOW, OFF_HIGH]
        // - driver layout
        //   - [PWM_A, IN1_A, IN2_A, IN1_B, IN2_B, PWM_B]
        //   - each driver entry is 4 bytes wide

        // datagram consists of the control register + data for 3 driver
        // -> 1 + 3 * 4 = 13 bytes
        let mut datagram = [0u8; 13];

        let control_register = Register::DriverBase as u8
            + 4 * match motor {
                Motor::A => 0x00, // first driver for motor A
                Motor::B => 0x03, // first driver for motor B
            };
        datagram[0] = control_register;

        let data = &mut datagram[1..];

        let pwm = match motor {
            Motor::A => &mut data[2..=3],   // PWM for motor A
            Motor::B => &mut data[10..=11], // PWM for motor B
        };
        let duty_cycle = ((speed.abs() as f32) as u32 * 4095 / 100) as u16;
        pwm.clone_from_slice(&duty_cycle.to_le_bytes());

        // if speed is 0, set both, IN1 and IN2, to high
        // this leeds to "short brake" instead of "off", see TB6612FNG
        // in "short break" the wheel stops immediately to spin
        // in "off" the wheel continuous to coast and is slowly stopping
        if speed >= 0 {
            let in1_off = match motor {
                Motor::A => &mut data[6..=7], // IN1 for motor A
                Motor::B => &mut data[2..=3], // IN2 for motor B
            };
            in1_off.clone_from_slice(&[0xFF, 0x0F]);
        }
        if speed <= 0 {
            let in2_off = match motor {
                Motor::A => &mut data[10..=11], // IN2 for motor A
                Motor::B => &mut data[6..=7],   // IN2 for motor B
            };
            in2_off.clone_from_slice(&[0xFF, 0x0F]);
        }

        self.bus.write(&datagram)?;

        Ok(())
    }
}
