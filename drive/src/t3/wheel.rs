use super::ecu::{Ecu, Motor};

use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug)]
pub(super) struct Wheel {
    ecu: Option<Rc<RefCell<Ecu>>>,
    motor: Motor,
    current_speed: i8,
}

impl Wheel {
    pub(super) fn new(ecu: Option<Rc<RefCell<Ecu>>>, motor: Motor) -> Self {
        Self {
            ecu,
            motor,
            current_speed: 0,
        }
    }

    pub(super) fn run(&mut self, speed: f64) {
        // the motor starts to drive larry at about 20% therefore remap to the upper 80%
        const REMAP_OFF_THRESHOLD: f64 = 5.0;
        const REMAP_OFFSET: f64 = 20.0;
        const REMAP_RANGE: f64 = (100.0 - REMAP_OFFSET) / 100.0;
        let speed_abs = speed.abs();
        let speed = if speed_abs < REMAP_OFF_THRESHOLD {
            0.0f64
        } else {
            num_traits::clamp(speed_abs * REMAP_RANGE + REMAP_OFFSET, 0f64, 100f64).copysign(speed)
        };

        if let Some(ecu) = self.ecu.as_mut() {
            if let Ok(mut ecu) = ecu.try_borrow_mut() {
                if speed as i8 != self.current_speed {
                    let _ = ecu.set_speed(self.motor, speed as i8);
                    self.current_speed = speed as i8;
                }
            }
        }
    }

    pub(super) fn stop(&mut self) {
        self.run(0.);
    }
}
