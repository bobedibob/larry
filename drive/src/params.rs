use clap::Parser;

/// Drive daemon for larry
#[derive(Parser, Debug)]
#[command(author, version, about, long_about)]
pub struct Params {
    // /// The PWM frequency to drive the servo motors.
    // #[arg(short, long, default_value = "3921Hz")]
    // pub frequency: Frequency,
}
