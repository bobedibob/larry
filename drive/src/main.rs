mod params;

mod vehicle;
use crate::vehicle::{DummyVehicle, Vehicle};
mod t3;
use crate::t3::T3;

use etm::server::{MessageProcessing, Server};
use etm::Service;
use rpc::drive::{ProtocolVersion, Request, Response, SERVICE_CONNECTION_REQUEST_PORT, SERVICE_ID};

use clap::Parser;
use rppal::system::DeviceInfo;

use std::sync::{Arc, Mutex};

struct LarryDrive {
    vehicle: Arc<Mutex<dyn Vehicle>>,
}

impl LarryDrive {
    fn new() -> Self {
        let params = params::Params::parse();
        log::debug!("{:?}", params);

        let vehicle: Arc<Mutex<dyn Vehicle>> = match DeviceInfo::new() {
            Ok(device_info) => {
                log::info!(
                    "Model: {} (SoC: {})",
                    device_info.model(),
                    device_info.soc()
                );

                Arc::new(Mutex::new(
                    T3::new().expect("failed to create a T3 vehicle"),
                ))
            }
            Err(error) => {
                log::warn!("Not a RaspberryPI, using Dummy driver. Error: {:?}", error);

                Arc::new(Mutex::new(DummyVehicle {}))
            }
        };

        LarryDrive { vehicle }
    }
}

impl MessageProcessing for LarryDrive {
    type Rq = rpc::drive::Request;
    type Rsp = rpc::drive::Response;
    type E = rpc::drive::Error;

    fn new() -> Arc<Self> {
        Arc::new(LarryDrive::new())
    }

    fn execute(&self, _connection_id: u32, rpc: Self::Rq) -> Result<Self::Rsp, Self::E> {
        match rpc {
            Request::Ping() => Ok(Response::Pong()),
            Request::Info() => {
                let version_firmware =
                    env!("CARGO_PKG_NAME").to_string() + " - v" + env!("CARGO_PKG_VERSION");

                Ok(Response::Info(version_firmware))
            }
            Request::Move {
                speed,
                direction,
                rotation_speed,
            } => {
                log::debug!(
                    "set speed: speed({:.1}%) direction({:.1}° rotation_speed({:.1})%)",
                    speed,
                    direction,
                    rotation_speed
                );
                self.vehicle
                    .lock()
                    .expect("getting lock")
                    .run(speed, direction, rotation_speed);
                Ok(Response::Speed())
            }
            Request::Stop() => {
                log::debug!("stopping vehicle");
                self.vehicle.lock().expect("getting lock").stop();
                Ok(Response::Stop())
            }
        }
    }
}

fn main() -> std::io::Result<()> {
    larry_log::init("info", larry_log::Target::DuplicateToConsole);

    let server = Server::<LarryDrive>::new(
        SERVICE_CONNECTION_REQUEST_PORT,
        Service::entity(SERVICE_ID.to_string(), ProtocolVersion::entity().version()),
    );
    server.run()?;

    Ok(())
}
