pub trait Vehicle: Send {
    fn run(&mut self, _speed: f64, _direction: f64, _rotation: f64) {}

    fn stop(&mut self) {}
}

pub struct DummyVehicle {}

impl Vehicle for DummyVehicle {}
