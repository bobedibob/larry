# RPC for larry

The RPC enums are defined in lib.rs.

## Ping/Pong

A simple message to test the connection.

## Info

Request a string with the version info.

## StreamingPort

Request a streaming port for the camera images. The Response data is the port which will be used to stream the data.
The port has to be opened within two seconds. After two seconds a new request must be issued.

The streaming data on the port hast the following layout:

| 8 bytes length of remaining message | 8 bytes frame number starting with 0 | 8 byte timestamp [us] | image data as u8 array | 8 bytes termination (0xAF FE DE AD) | 8 bytes length of remaining message | ...

Everything is sent in network order.
