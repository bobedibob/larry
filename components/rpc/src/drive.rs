/*
 * Copyright (C) 2018 Mathias Kraus <elboberido@m-hias.de> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

use serde::{Deserialize, Serialize};

pub const SERVICE_CONNECTION_REQUEST_PORT: u16 = 0xA2D2;

pub const SERVICE_ID: &str = "larry-drive";

pub struct ProtocolVersion {
    version: u32,
}

impl ProtocolVersion {
    pub fn entity() -> Self {
        ProtocolVersion {
            version: env!("CARGO_PKG_VERSION_MAJOR")
                .parse::<u32>()
                .unwrap_or(std::u32::MAX),
        }
    }

    pub fn version(&self) -> u32 {
        self.version
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub enum Request {
    Ping(),
    Info(),
    Move {
        speed: f64,          // range is 0..100%
        direction: f64,      // range is +-180°
        rotation_speed: f64, // range is +-100%
    },
    Stop(),
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub enum Response {
    Pong(),
    Info(String),
    Speed(),
    Stop(),
}

pub type Error = String;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
