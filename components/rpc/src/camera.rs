/*
 * Copyright (C) 2018 Mathias Kraus <elboberido@m-hias.de> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

use serde::{Deserialize, Serialize};

pub const SERVICE_CONNECTION_REQUEST_PORT: u16 = 0x2323;

pub const SERVICE_ID: &str = "larry-camera";

pub struct ProtocolVersion {
    version: u32,
}

impl ProtocolVersion {
    pub fn entity() -> Self {
        ProtocolVersion {
            version: env!("CARGO_PKG_VERSION_MAJOR")
                .parse::<u32>()
                .unwrap_or(std::u32::MAX),
        }
    }

    pub fn version(&self) -> u32 {
        self.version
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub enum Request {
    Ping(),
    Info(),
    StreamingPort(),
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub enum Response {
    Pong(),
    Info(String),
    StreamingPort { port: u16 },
}

pub type Error = String;

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct CameraStream<'a> {
    pub timestamp: u64,
    pub frame: &'a [u8],
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
