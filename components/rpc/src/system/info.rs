/*
 * Copyright (C) 2018 Mathias Kraus <elboberido@m-hias.de> - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

use serde::{Deserialize, Serialize};

pub const SERVICE_CONNECTION_REQUEST_PORT: u16 = 0x6666;

pub const SERVICE_ID: &str = "larry-system-info";

pub struct ProtocolVersion {
    version: u32,
}

impl ProtocolVersion {
    pub fn entity() -> Self {
        ProtocolVersion {
            version: env!("CARGO_PKG_VERSION_MAJOR")
                .parse::<u32>()
                .unwrap_or(std::u32::MAX),
        }
    }

    pub fn version(&self) -> u32 {
        self.version
    }
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub enum Request {
    Ping(),
    Info(),
    Uptime(),
    CPU(),
    Memory(),
    Network(),
    Temperature(),
    AllSystemInfo(),
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub enum Response {
    Pong(),
    Info(String),
    Uptime(u64),
    CPU(Vec<f32>),
    Memory(Memory),
    Network(Vec<Network>),
    Temperature(Vec<Temperature>),
    AllSystemInfo(AllSystemInfo),
}

pub type Error = String;

#[derive(Clone, Copy, Default, Serialize, Deserialize, PartialEq, Debug)]
pub struct Memory {
    pub total: u64, // Bytes
    pub used: u64,
    pub swap: u64,
    pub swap_used: u64,
}

#[derive(Clone, Default, Serialize, Deserialize, PartialEq, Debug)]
pub struct Network {
    pub interface_name: String,
    pub received: u64, // Bytes/s
    pub transmitted: u64,
    pub packets_received: u64,
    pub packets_transmitted: u64,
    pub total_errors_on_received: u64,
    pub total_errors_on_transmitted: u64,
}

#[derive(Clone, Default, Serialize, Deserialize, PartialEq, Debug)]
pub struct Temperature {
    pub label: String,
    pub value: f32, // °C
    pub max: f32,
    pub critical: Option<f32>,
}

#[derive(Clone, Default, Serialize, Deserialize, PartialEq, Debug)]
pub struct AllSystemInfo {
    pub uptime: u64,
    pub cpu: Vec<f32>,
    pub memory: Memory,
    pub network: Vec<Network>,
    pub temperature: Vec<Temperature>,
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
