use directories::ProjectDirs;
use flexi_logger::{
    style, Cleanup, Criterion, DeferredNow, Duplicate, FileSpec, FlexiLoggerError, Logger, Naming,
};
use yansi::Paint;

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Target {
    FileOnly,
    DuplicateToConsole,
}

pub fn init(default_level: &str, target: Target) {
    let project_dirs =
        ProjectDirs::from("org", "larry.robotics", "larry").expect("project directories");

    try_init(default_level, target, "/var/log/larry")
        .or_else(|_| {
            try_init(
                default_level,
                target,
                project_dirs
                    .data_dir()
                    .join("log")
                    .to_str()
                    .expect("log directory"),
            )
        })
        .expect("Logger started!");
}

pub fn try_init(
    default_level: &str,
    target: Target,
    log_directory: &str,
) -> Result<(), FlexiLoggerError> {
    let project_dirs =
        ProjectDirs::from("org", "larry.robotics", "larry").expect("project directories");

    let filespec = FileSpec::default().directory(log_directory);

    let duplicate = if target == Target::FileOnly {
        Duplicate::None
    } else {
        Duplicate::All
    };

    Logger::try_with_str(default_level)?
        .log_to_file(filespec)
        .append()
        .rotate(
            Criterion::Size(1024 * 1024),
            Naming::Timestamps,
            Cleanup::KeepLogFiles(5),
        )
        .format_for_files(flexi_logger::opt_format)
        .duplicate_to_stderr(duplicate)
        .set_palette("9;11;10;7;8".to_owned())
        .format_for_stderr(format)
        .start_with_specfile(
            project_dirs
                .config_dir()
                .join("logspec.toml")
                .to_str()
                .expect("logspec file"),
        )?;

    Ok(())
}

fn format(
    w: &mut dyn std::io::Write,
    now: &mut DeferredNow,
    record: &log::Record,
) -> Result<(), std::io::Error> {
    let level = record.level();

    let level_text = match level {
        log::Level::Error => "[Error]",
        log::Level::Warn => "[Warn ]",
        log::Level::Info => "[Info ]",
        log::Level::Debug => "[Debug]",
        log::Level::Trace => "[Trace]",
    };

    write!(
        w,
        "{} {} [{}] {}",
        Paint::fixed(8, now.now().format("%Y-%m-%d %H:%M:%S%.3f")).dimmed(),
        // style(level, level_text),
        style(level).paint(level_text),
        record.module_path().unwrap_or("<unnamed>"),
        &record.args()
    )
}
